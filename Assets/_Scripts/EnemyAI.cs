﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour {

    public Transform target;
    NavMeshAgent agent;

	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.Find("FPSController").transform;
	}
	
	// Update is called once per frame
	void Update () {
        agent.SetDestination(target.position);
        this.transform.LookAt(2* transform.position - target.position);
	}
}
