﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour {

    public int Health = 15;
    public int Speed;
    public int Damage;
    public GameManager GM_Object;

	// Use this for initialization
	void Start () {
        GM_Object = GameObject.Find("_GameManager").GetComponent<GameManager>();
        
	}
	
	// Update is called once per frame
	void Update () {
		if(Health <= 0) {
            Destroy(this.gameObject);
            GM_Object.Score = GM_Object.Score + 200;
        }
	}

    //Check for enemy level via tag to determine health


    //Do the same for speed

    //Again for damage

    //Apply Damage to self
    public void OnTriggerEnter(Collider other) {
        if (other.tag == "Bullet1") {
            Health = Health - 15;
            //Destroy(other);
        }
        if (other.tag == "Bullet2") {
            Health = Health - 25;
            Destroy(other);
        }
        if (other.tag == "bullet3") {
            Health = Health - 45;
            Destroy(other);
        }
        //apply damage to enemy and get points
        if (other.tag == "Player") {
            
            if (this.tag == "tier1") {
                other.GetComponent<PlayerDamage>().playerHealth = other.GetComponent<PlayerDamage>().playerHealth - 10;
            }
            if (this.tag == "tier2") {
                other.GetComponent<PlayerDamage>().playerHealth = other.GetComponent<PlayerDamage>().playerHealth - 15;
            }
            if (this.tag == "tier3") {
                other.GetComponent<PlayerDamage>().playerHealth = other.GetComponent<PlayerDamage>().playerHealth - 20;
            }
        }

    }

}
