﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigiSplash : MonoBehaviour {
    public int weaponSelected = 0;
    public GameObject figi;
    public GameObject figiSplash;
    public GameObject cd;
    public GameObject cd_Bullet;
    public GameObject sword;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Q)) {
            weaponSelected++;
            if(weaponSelected == 4) {
                weaponSelected = 1;
            }
        }
		if(weaponSelected == 1) {
            figi.SetActive(true);
            cd.SetActive(false);
            cd_Bullet.SetActive(false);
            if (Input.GetKeyDown(KeyCode.Mouse0)){
                figiSplash.SetActive(true);
            }
            if (Input.GetKeyUp(KeyCode.Mouse0)) {
                figiSplash.SetActive(false);
            }
        }
        if(weaponSelected == 2) {
            cd.SetActive(true);
            cd_Bullet.SetActive(true);
            figi.SetActive(false);
        }
	}
}
