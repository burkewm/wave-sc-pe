﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour {


    public int roundCount = 1;
    public bool roundStart;
    public int Score;
    public int spawnCount = 0;
    public GameObject PauseMenu;
    FirstPersonController FPSControl;
    public PlayerDamage PlayerDamage;
    public GameObject ScoreBoard;
    public VHSPostProcessEffect VHSeffect;
	// Use this for initialization
	void Start () {
        Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape) && PlayerDamage.playerHealth > 0) {
            PauseMenu.SetActive(true);
           // FPSControl.enabled = false;
            Time.timeScale = 0.0f;
        }
        ScoreBoard.GetComponent<Text>().text = Score.ToString();
	}

    //Fuctions to do
    public void Exit() {
        Application.Quit();
    }

    public void Resume() {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        //FPSControl.enabled = true;
    }

    public void Restart() {
        Time.timeScale = 1;
        SceneManager.LoadScene(1);
    }

    public void DisableVHS() {
        VHSeffect.enabled = false;
    }
    public void EnableVHS() {
        VHSeffect.enabled = true;
    }
    //Start the round

    //Increase the Round Count
}
