﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamage : MonoBehaviour {

    public int playerHealth = 100;
    public GameObject RestartMenu;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Killer Player if helath goes to 0
        if(playerHealth <= 0) {
            Time.timeScale = 0;
            RestartMenu.SetActive(true);
            //SaveScore
            //Load back to menu
        }
	}
}
