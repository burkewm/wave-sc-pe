﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour {

    public GameManager GM_Check;
    public List<GameObject> enemyType;
    public int roundCount;

    public int randomnbr;

    public bool canSpawn = true;

	// Use this for initialization
	void Start () {
        //Add all enemy types from the Resources folder into the enemytype array
        enemyType = Resources.LoadAll<GameObject>("Enemy").ToList();
        
    }
	
	// Update is called once per frame
	void Update () {
        //Generate the enemy list

        //Check The GM for what round it is
        if (GM_Check.roundCount == 1 && canSpawn == true) {
            //Spawn enemy
            
            canSpawn = false;
            StartCoroutine(waitToSpawn());
        }
    }


    IEnumerator waitToSpawn() {
        GM_Check.spawnCount++;
        yield return new WaitForSeconds(Random.Range(4, 20));
        randomnbr = Random.Range(0, enemyType.Count);
        Instantiate(enemyType[randomnbr], this.transform.position, this.transform.rotation);
        canSpawn = true;
    }
}
